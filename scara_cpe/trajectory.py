#!/usr/bin/env python

""" Trajectory 

Robot Trajectory Interpolation

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Raphael LEBER"
__copyright__ = "Copyright 2024, CPE Lyon"


import numpy as np
from numpy import sqrt, sign

# if __package__ is None or __package__ == '':
#     from tools.calc import Calc
# else:
from .tools.calc import Calc
    
class Trajectory:
    
    def __init__(self, inter_type, qi=[66, 100], qf = [6, 60], kv = [45, 45], ka = [60, 90]):
          
        self.inter_type = inter_type
        
        self.calc_qp = Calc()
        self.calc_qpp = Calc()        
        
        self.qi = np.deg2rad(qi)
        self.qf = np.deg2rad(qf)
        self.D = self.qf - self.qi
        self.kv = np.deg2rad(np.array(kv))
        self.ka = np.deg2rad(np.array(ka))
        
        #TODO student: set tf and other parameters
             

        
    def get_inter_values(self):
        
        return self.qi, self.qf, self.D, self.kv, self.ka, self.tf, self.Tau, self.l  
    
    

    def update_trajectory(self, t):
        
        q = np.zeros_like(self.D)
        

        #TODO students : update q, qp, qpp


        qp = self.calc_qp.deriv(q, t)
        qpp = self.calc_qpp.deriv(qp, t) 
        
        return q, qp, qpp

         

