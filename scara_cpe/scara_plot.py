#!/usr/bin/env python

""" Scara Plot

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Raphael LEBER"
__copyright__ = "Copyright 2024, CPE Lyon"


import os
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backend_bases import MouseButton
import matplotlib.patches as patches
from matplotlib.widgets import TextBox, Slider, Button, RadioButtons, CheckButtons, Cursor, SpanSelector
import matplotlib.lines as mlines


# /!\ matplotlib utilise tkinter a installer pour la bonne version de python3 (e.g. sudo apt-get install python3.9-tk)

import matplotlib.image as mpimg

class ScaraPlot():

    def __init__(self, robot):
        
        self.robot = robot  
        #self.loop(robot)
        
        self.T = 0
        self.t = 0
        self.dt = 0.03
        
        self.sim_type = ""
        
        self.curves_data = {
            "x":[],
            "y":[],
            "x_p":[],
            "y_p":[],  
            "x_pp":[],
            "y_pp":[],               
            "theta1":[],
            "theta2":[],
            "theta1_p":[],
            "theta2_p":[],  
            "theta1_pp":[],
            "theta2_pp":[],                       
            "t":[],              
        }


        self.curves_meta = {
            "q": {
                "data_abs"  : ["t", "t"],
                "data_ord"  : ["theta1", "theta2"],
                "label" : ["$\\theta_1$", "$\\theta_2$"],
                "ylabel" : "Angle (rad)",
                "xlabel" : "Time (s)"
            },
            
            "q_p" : {
                "data_abs"  : ["t", "t"],
                "data_ord"  : ["theta1_p", "theta2_p"],
                "label" : ["$\dot{\\theta}_1$", "$\dot{\\theta}_2$"],
                "ylabel" : "Angular speed ($rad/s$)",
                "xlabel" : "Time (s)"
            },  
                                    
            "q_pp" : {
                "data_abs"  : ["t", "t"],
                "data_ord"  : ["theta1_pp", "theta2_pp"],
                "label" : ["$\ddot{\\theta}_1$", "$\ddot{\\theta}_2$"],
                "ylabel" : "Angular acceleration ($rad/s²$)",
                "xlabel" : "Time (s)"
            },                   
            
            "X": {
                "data_abs"  : ["t", "t"],
                "data_ord"  : ["x", "y"],
                "label" : ["x", "y"],
                "ylabel" : "Position (m)",
                "xlabel" : "Time (s)"
            },
            
            "V": {
                "data_abs"  : ["t", "t"],
                "data_ord"  : ["x_p", "y_p"],
                "label" : ["$\dot{x}$", "$\dot{y}$"],
                "ylabel" : "Speed (m/s)",
                "xlabel" : "Time (s)"
            }            
        }        

        self.plot_alive = True
        
        self.coords = (0, 0)
        
        plt.switch_backend('tkagg')
        plt.connect('button_press_event', self.on_click) 
        plt.gcf().canvas.mpl_connect('close_event', self.on_close)
        
        self.fig_curves = plt.figure(3)
        manager = plt.get_current_fig_manager()
        manager.window.title("Curves")         
        
        # Create a new figure for the robot plot
        self.fig_robot = plt.figure(1)
        manager = plt.get_current_fig_manager()
        manager.window.title("Robot Display")
        manager.window.geometry("+800+100")     
        
        self.draw_robot(0, 0)
        
        # Create buttons for MGI and MGD
        self.create_buttons()
        
        

    def perform_get_q(self, event):
        print("Perform Get q")        
        
        self.slider_th1.set_val( self.robot.theta1 )
        self.slider_th2.set_val( self.robot.theta2 )
        self.slider_th1.set_active(True)
        self.slider_th2.set_active(True)
        
        
    def perform_get_X(self, event):
        print("Perform Get X")        
        
        self.slider_x.set_val( self.robot.x )
        self.slider_y.set_val( self.robot.y )
        self.slider_x.set_active(True)
        self.slider_y.set_active(True)        
 
    
    def perform_mgd(self, event):
        print("Perform MGD")    
        
        try:
            q = (self.slider_th1.val, self.slider_th2.val)
            X = self.robot.MGD(q)
            
            self.slider_x.set_val(X[0])
            self.slider_y.set_val(X[1])
            self.slider_x.set_active(True)
            self.slider_y.set_active(True)
            
            self.robot.theta1 = q[0]
            self.robot.theta2 = q[1]
        
            print("Valeurs de X calculées:", X)
            
        except Exception as e:
            print("Erreur lors du calcul de X:", e)                
        

    def perform_mgi(self, event):
        print("Perform MGI")   
        
        try:
            q = self.robot.MGI((self.slider_x.val, self.slider_y.val))
            
            self.slider_th1.set_val(q[0])
            self.slider_th2.set_val(q[1])
            self.slider_th1.set_active(True)
            self.slider_th2.set_active(True)
            
            self.robot.theta1 = q[0]
            self.robot.theta2 = q[1]
        
            print("Valeurs de q calculées:", q)
            
        except Exception as e:
            print("Erreur lors du calcul de q:", e)

    
        
    def perform_mcd(self, event):
        print("Perform MCD")     
        
        self.sim_type = "MCD"    
        
        try:
            for key in self.curves_data:
                if isinstance(self.curves_data[key], list):
                    self.curves_data[key].clear()               
            self.T = float(self.textbox_time.text)
            self.t = 0
            
        except Exception as e:
            print("The time must be a number : ", e)           

    
    
    
    def perform_mci(self, event):
        print("Perform MCI")       
        
        self.sim_type = "MCI"
        
        try:
            for key in self.curves_data:
                if isinstance(self.curves_data[key], list):
                    self.curves_data[key].clear()      
                          
            self.T = float(self.textbox_time.text)
            self.t = 0           
            
        except Exception as e:
            print("The time must be a number : ", e)      
            
            
         
    def perform_gen_traj(self, event):
        print("Perform trajectory")   
        
        try:
            for key in self.curves_data:
                if isinstance(self.curves_data[key], list):
                    self.curves_data[key].clear()      
                          
            self.robot.calc_qp.reset()
            self.robot.calc_qpp.reset()
            self.T = float(self.textbox_time.text)
            self.t = 0           
            
        except Exception as e:
            print("The time must be a number : ", e)         
        


    def perform_inter_pol1(self, event):
        self.sim_type = "inter_pol1"
        self.perform_gen_traj(event)

    def perform_inter_pol3(self, event):
        self.sim_type = "inter_pol3"
        self.perform_gen_traj(event)  

    def perform_inter_pol5(self, event):
        self.sim_type = "inter_pol5"
        self.perform_gen_traj(event)   

    def perform_inter_bang(self, event):
        self.sim_type = "inter_bang"
        self.perform_gen_traj(event)      

    def perform_inter_trap(self, event):
        self.sim_type = "inter_trap"
        self.perform_gen_traj(event)     

    def perform_inter_sync(self, event):
        self.sim_type = "inter_sync"
        self.perform_gen_traj(event)       

                
    
    def close_figures(self):
        plt.figure(1)
        plt.close()
        plt.figure(2)
        plt.close()
        plt.figure(3)
        plt.close()        
    

    def create_buttons(self):
        # Create a new figure for the buttons
        self.fig_buttons = plt.figure(2)
        manager = plt.get_current_fig_manager()
        manager.window.title("Control Panel") 
        manager.window.geometry("+100+100")
        
        current_dir = os.path.dirname(os.path.abspath(__file__))
        logo_path = os.path.join(current_dir, '../assets/images/logo.png')  
        img = mpimg.imread(logo_path)

        # Définir la position et la taille de l'image
        ax_image = plt.axes([0.88, 0.9, 0.1, 0.1]) # [left, bottom, width, height]
        ax_image.imshow(img)
        ax_image.axis('off') # Pour cacher les axes     
        
        # Create buttons to get robot states
        ax_get_q = plt.axes([0.05, 0.9, 0.25, 0.05])  
        ax_get_X = plt.axes([0.6, 0.9, 0.25, 0.05])    
        
        # Create sliders for angles
        ax_theta1 = plt.axes([0.05, 0.85, 0.25, 0.05])
        ax_theta2 = plt.axes([0.05, 0.8, 0.25, 0.05])

        # Create buttons for MGI and MGD
        ax_mgd = plt.axes([0.425, 0.85, 0.1, 0.05])        
        ax_mgi = plt.axes([0.425, 0.8, 0.1, 0.05])

        # Create new sliders
        ax_slider1 = plt.axes([0.6, 0.85, 0.3, 0.05])
        ax_slider2 = plt.axes([0.6, 0.8, 0.3, 0.05])
        
        self.button_get_q = Button(ax_get_q, 'Get robot $\\theta_1$ $\\theta_2$')
        self.button_get_X = Button(ax_get_X, 'Get robot x y')
        self.button_mgi = Button(ax_mgi, '<- MGI')
        self.button_mgd = Button(ax_mgd, 'MGD ->')
        self.slider_th1 = Slider(ax_theta1, '$\\theta_1$', *self.robot.bounds_theta1, valinit=0, valstep=0.1)
        self.slider_th2 = Slider(ax_theta2, '$\\theta_2$', *self.robot.bounds_theta2, valinit=0, valstep=0.1)
        self.slider_x = Slider(ax_slider1, 'x', -15, 15, valinit=0.0)
        self.slider_y = Slider(ax_slider2, 'y', -self.robot.L2_x, self.robot.L1_x + self.robot.L2_x, valinit=0.0)

        # Connect button presses to functions
        self.button_mgi.on_clicked(self.perform_mgi)
        self.button_mgd.on_clicked(self.perform_mgd)
        self.button_get_q.on_clicked(self.perform_get_q)
        self.button_get_X.on_clicked(self.perform_get_X)
        #self.slider_th1.on_changed(self.perform_theta1)
        #self.slider_th2.on_changed(self.perform_theta2)

            
        # Ligne de séparation ------------------------------------------------------------
        ax_line = plt.axes([0.1, 0.7375, 0.8, 0.02])  # [left, bottom, width, height]
        ax_line.axis('off') # Pour cacher les axes    
        line = mlines.Line2D([0.1, 0.9], [0.6, 0.6], color='gray', linestyle='--', linewidth=1)
        ax_line.add_line(line)

        # Create sliders and buttons for MCD and MCI
        ax_theta1_p = plt.axes([0.05, 0.65, 0.25, 0.05])
        ax_theta2_p = plt.axes([0.05, 0.6, 0.25, 0.05])
        ax_x_p = plt.axes([0.6, 0.65, 0.3, 0.05])
        ax_y_p = plt.axes([0.6, 0.6, 0.3, 0.05])
        ax_mcd = plt.axes([0.425, 0.65, 0.1, 0.05])
        ax_mci = plt.axes([0.425, 0.6, 0.1, 0.05])
        
        ax_time = plt.axes([0.425, 0.55, 0.1, 0.05])
        
        self.button_mci = Button(ax_mci, '<- MCI')
        self.button_mcd = Button(ax_mcd, 'MCD ->')
        self.slider_th1_point = Slider(ax_theta1_p, '$\dot{\\theta}_1$', -0.5, 0.5, valinit=0, valstep=0.001)
        self.slider_th2_point = Slider(ax_theta2_p, '$\dot{\\theta}_2$', -0.5, 0.5, valinit=0, valstep=0.001)
        self.slider_x_point = Slider(ax_x_p, '$\dot{x}$', -5, 5, valinit=0.0, valstep=0.01)
        self.slider_y_point = Slider(ax_y_p, '$\dot{y}$', -5, 5, valinit=0.0, valstep=0.01)       
        
        self.textbox_time = TextBox(ax_time, 'Time:', initial='2')
        
        self.button_mci.on_clicked(self.perform_mci)
        self.button_mcd.on_clicked(self.perform_mcd)        

        
        # Ligne de séparation ------------------------------------------------------------
        ax_line2 = plt.axes([0.1, 0.4875, 0.8, 0.02])  # [left, bottom, width, height]
        ax_line2.axis('off') # Pour cacher les axes    
        line2 = mlines.Line2D([0.1, 0.9], [0.6, 0.6], color='gray', linestyle='--', linewidth=1)
        ax_line2.add_line(line2)             
            
            
        ax_inter_pol1 = plt.axes([0.05, 0.4, 0.125, 0.05])
        ax_inter_pol3 = plt.axes([0.200, 0.4, 0.125, 0.05])
        ax_inter_pol5 = plt.axes([0.350, 0.4, 0.125, 0.05])
        ax_inter_bang = plt.axes([0.500, 0.4, 0.125, 0.05])
        ax_inter_trap = plt.axes([0.650, 0.4, 0.125, 0.05])
        ax_inter_trap_sync = plt.axes([0.800, 0.4, 0.125, 0.05])

        self.button_inter_pol1  = Button(ax_inter_pol1, 'Poly. 1')
        self.button_inter_pol3  = Button(ax_inter_pol3, 'Poly. 3')
        self.button_inter_pol5  = Button(ax_inter_pol5, 'Poly. 5')
        self.button_inter_bang  = Button(ax_inter_bang, 'Bang Bang')        
        self.button_inter_trap  = Button(ax_inter_trap, 'Trapeze')   
        self.button_inter_sync  = Button(ax_inter_trap_sync, 'Trap. Sync')   
        
        self.button_inter_pol1.on_clicked(self.perform_inter_pol1)
        self.button_inter_pol3.on_clicked(self.perform_inter_pol3)
        self.button_inter_pol5.on_clicked(self.perform_inter_pol5)
        self.button_inter_bang.on_clicked(self.perform_inter_bang)
        self.button_inter_trap.on_clicked(self.perform_inter_trap)
        self.button_inter_sync.on_clicked(self.perform_inter_sync)
        
        
        # Ligne de séparation |||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
        ax_line2 = plt.axes([0.575, 0.1, 0.02, 0.2])  # [left, bottom, width, height]
        ax_line2.axis('off') # Pour cacher les axes    
        line2 = mlines.Line2D([0.1, 0.1], [0.1, 0.3], color='gray', linestyle='--', linewidth=1)
        ax_line2.add_line(line2)     
        
        
        # Return to the original figure for drawing the robot
        plt.figure(1)

        
    # Fonction pour calculer les positions des articulations du robot
    def forward_kinematics(self, theta1, theta2):
        x1 = self.robot.L1_x * np.cos(theta1)
        y1 = self.robot.L1_x * np.sin(theta1)
        x2 = x1 + self.robot.L2_x * np.cos(theta1 + theta2)
        y2 = y1 + self.robot.L2_x * np.sin(theta1 + theta2)
        return x1, y1, x2, y2        
        
        
    def draw_robot(self, theta1, theta2):
        plt.cla() # Effacer la figure à chaque fois pour éviter le superposition
        x1, y1, x2, y2 = self.forward_kinematics(theta1, theta2)
        
        # Dessiner le premier bras
        plt.plot([0, x1], [0, y1], 'b-', lw=2)
        # Dessiner le deuxième bras
        plt.plot([x1, x2], [y1, y2], 'r-', lw=2)
        # Dessiner les articulations
        plt.plot(0, 0, 'ko') # Origine
        plt.plot(x1, y1, 'ko') # Articulation 1
        plt.plot(x2, y2, 'ko') # Articulation 2
        
        self.robot.x = x2
        self.robot.y = y2
        
        self. draw_robot_variables(theta1, theta2, x2, y2)
        
        
        
    def draw_robot_workspace(self):
        # Dessiner un demi-cercle enveloppe supérieure
        circle_radius = self.robot.L1_x + self.robot.L2_x 
        circle_center = (0, 0)
        circle = patches.Arc(circle_center, circle_radius*2, circle_radius*2, theta1=0, theta2=180, color='green')
        plt.gca().add_patch(circle)     
        
        # Dessiner un demi-cercle enveloppe inférieure
        circle_radius = self.robot.L1_x - self.robot.L2_x 
        circle_center = (0, 0)
        circle = patches.Arc(circle_center, circle_radius*2, circle_radius*2, theta1=0, theta2=180, color='green')
        plt.gca().add_patch(circle)           
        # Dessiner un demi-cercle enveloppe basse gauche
        circle_radius = 2*self.robot.L2_x  
        circle_center = (-self.robot.L1_x, 0)
        circle = patches.Arc(circle_center, circle_radius, circle_radius, theta1=180, theta2=360, color='green')
        plt.gca().add_patch(circle)   
        # Dessiner un demi-cercle enveloppe basse droit       
        circle_center = (self.robot.L1_x, 0)
        circle = patches.Arc(circle_center, circle_radius, circle_radius, theta1=180, theta2=360, color='green')
        plt.gca().add_patch(circle)                
        
    
    def draw_robot_variables(self, theta1, theta2, x, y):
        
        plt.text(0, -2.5, '$\\theta_1={:.1f}$°'.format(np.degrees(theta1)), ha='center', va='bottom')
        plt.text(0, -3.5, '$\\theta_2={:.1f}$°'.format(np.degrees(theta2)), ha='center', va='bottom')
        plt.text(0, -5, 'x={:.1f}'.format(x), ha='center', va='bottom')
        plt.text(0, -6, 'y={:.1f}'.format(y), ha='center', va='bottom')
        
        plt.xlim(-15, 15)
        plt.ylim(-7, 15)
        plt.gca().set_aspect('equal', adjustable='box')
        plt.title('RR Robot Pose')        
        
        self.draw_robot_workspace()
        
    def loop(self):
        while(self.plot_alive):
            #print("-------------------------------------")
           
            plt.figure(1)
            
            if(self.t <= (self.T )):
                
                if self.sim_type == "MCD" :

                    q = [self.robot.theta1,self.robot.theta2]
                    Vq = (self.slider_th1_point.val, self.slider_th2_point.val)
                    V = self.robot.MCD(q, Vq)
                    
                    self.slider_x_point.set_val(V[0])
                    self.slider_y_point.set_val(V[1])
                    self.slider_x_point.set_active(True)
                    self.slider_y_point.set_active(True)
                    
                    q[0] = q[0] + Vq[0]*self.dt
                    q[1] = q[1] + Vq[1]*self.dt
                    
                    self.robot.theta1 = q[0]
                    self.robot.theta2 = q[1]
                    
                    self.curves_data["x_p"].append(V[0])
                    self.curves_data["y_p"].append(V[1])   
                    for i in range(len(q)):
                        self.curves_data[f"theta{i+1}_p"].append(Vq[i])                                        
                                    
                
                    #print("Valeurs de V calculées:", V)
                    
                elif self.sim_type == "MCI" :
                    
                    q = [self.robot.theta1,self.robot.theta2]
                    V = (self.slider_x_point.val, self.slider_y_point.val)
                    Vq = self.robot.MCI(q, V)
                    
                    self.slider_th1_point.set_val(Vq[0])
                    self.slider_th2_point.set_val(Vq[1])
                    self.slider_th1_point.set_active(True)
                    self.slider_th2_point.set_active(True)
                    
                    q[0] = q[0] + Vq[0]*self.dt
                    q[1] = q[1] + Vq[1]*self.dt
                    
                    self.robot.theta1 = q[0]
                    self.robot.theta2 = q[1]
                    
                    self.curves_data["x_p"].append(V[0])
                    self.curves_data["y_p"].append(V[1])   
                    for i in range(len(q)):
                        self.curves_data[f"theta{i+1}_p"].append(Vq[i])                        
                    

                    #print("Valeurs de Vq calculées:", Vq)  
                    
                elif  "inter" in self.sim_type : 
                    
                    q, qp, qpp, tf_max = self.robot.update_trajectory(self.sim_type, self.t)
                    
                    if self.T > self.dt:
                        self.T = tf_max * 1.05
                    
                    self.robot.theta1 = q[0]
                    self.robot.theta2 = q[1]
                    
                    for i in range(len(q)):
                        self.curves_data[f"theta{i+1}_p"].append(qp[i])    
                        self.curves_data[f"theta{i+1}_pp"].append(qpp[i])               
                        #print("qp = {}   -   qpp = {}".format(qp, qpp))   
                
                    #print("Valeurs de q calculées:", q)  
                    
                    
                
                self.t = self.t + self.dt
                
         
            elif(self.T >= self.dt):
                
                if "inter" in self.sim_type :

                    self.plot_curves(["q", "q_p", "q_pp"])
                
                elif self.sim_type == "MCD" :    
                    
                    self.plot_curves(["q", "X", "V"])
                    
                elif self.sim_type == "MCI" :    
                    
                    self.plot_curves(["q", "q_p", "X"])                    
                    
                else :
                    
                    self.plot_curves(["q", "X"])
                
                plt.figure(1)
                
                self.T = 0
                self.t = 0    
                

            self.draw_robot(self.robot.theta1, self.robot.theta2)

            # Append values for plotting
            
            self.curves_data["theta1"].append(self.robot.theta1)
            self.curves_data["theta2"].append(self.robot.theta2)
            self.curves_data["x"].append(self.robot.x)
            self.curves_data["y"].append(self.robot.y)
            self.curves_data["t"].append(self.t)                    
            
            plt.pause(self.dt) # Pause pour montrer chaque frame  
        #plt.show()
        
        
        
    def plot_curves(self, label_list):
        n = len(label_list)
        
        try:
            self.fig_curves.clf()
        except:
            self.fig_curves = plt.figure(3)
            manager = plt.get_current_fig_manager()
            manager.window.title("Curves")  
    
        axs = self.fig_curves.subplots(n, 1, sharex=True)  
        
        for i,label in enumerate(label_list):
            
            c_abs = self.curves_meta[label]["data_abs"]
            c_ord = self.curves_meta[label]["data_ord"]
            c_xlabel = self.curves_meta[label]["xlabel"]
            c_ylabel = self.curves_meta[label]["ylabel"]
            c_label = self.curves_meta[label]["label"]
            
            for j,_ in enumerate(c_abs):
                axs[i].plot(self.curves_data[c_abs[j]], self.curves_data[c_ord[j]], label=c_label[j])
                axs[i].set_ylabel(c_xlabel)
                axs[i].set_xlabel(c_ylabel)
                axs[i].legend()        
            
        plt.show(block=False)

    #------------- MatPloLib Callbacks -------------------
        
    def on_click(self, event):
        
        if event.button is MouseButton.LEFT:
            
            self.coords = event.xdata, event.ydata
            print("clicked on coordinates ( x={:.2f},  y={:.2f} )".format(event.xdata, event.ydata))            
            
            if(event.canvas.figure == self.fig_robot):

                if (event.xdata > 0):
                    th2 = -1.57
                    if( event.ydata > 0):
                        th1 = 1.57
                    else :
                        th1 = 0
                else :
                    th2 = 1.57
                    if( event.ydata > 0):
                        th1 = 1.57
                    else :
                        th1 = 3.14     
                                
                initial = (th1, th2)
            
                self.robot.x, self.robot.y = self.robot.num_mgi_solver.solve(self.coords, initial)
                
                self.robot.theta1, self.robot.theta2 = self.robot.sat_to_bounds( self.robot.x, self.robot.y )


    def on_close(self, event):
        print('Window closed!')    
        self.plot_alive = False
