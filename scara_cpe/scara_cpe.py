#!/usr/bin/env python

""" Scara CPE

Scara CPE robot simulator for practical work at CPE Lyon

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Raphael LEBER"
__copyright__ = "Copyright 2024, CPE Lyon"


from .scara_model import ScaraModel
from .scara_plot import ScaraPlot
import signal
import sys


class ScaraCpe():

    def __init__(self):
        
        signal.signal(signal.SIGINT, self.ctrl_c_handler)
        
        self.scara = ScaraModel()
        
        self.plot = ScaraPlot(self.scara)  
        self.plot.loop()


    def ctrl_c_handler(self, signal, frame):
        
        print("Ctrl+C detected, exiting...")
        self.plot.close_figures()
        sys.exit(0)


if __name__ == "__main__":
    s = ScaraCpe()




