#!/usr/bin/env python

__author__ = "Raphael LEBER"
__copyright__ = "Copyright 2024, CPE Lyon"

from sympy import cos, sin
from scipy.optimize import fsolve
import numpy as np
import random


class ScaraNumSolver():
    
    def __init__(s, L1, L2) -> None:    
        s.L1 = L1
        s.L2 = L2

    def __equations(s, variables):
        th1, th2 = variables
        eq1 = s.L1*cos(th1) + s.L2*cos(th1 + th2) - s.x
        eq2 = s.L1*sin(th1) + s.L2*sin(th1 + th2) - s.y
        return [eq1, eq2]
    
    def solve(s, X, initial_angles):
        
        s.x, s.y = X

        return fsolve(s.__equations, initial_angles)
        

