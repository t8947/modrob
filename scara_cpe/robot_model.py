#!/usr/bin/env python

""" Robot Model

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Raphael LEBER"
__copyright__ = "Copyright 2024, CPE Lyon"

import numpy as np



class RobotModel():

    def __init__(self, chain, dh_kk_parameters):
        
        self.chain = chain
        self.dh_kk_parameters = dh_kk_parameters
        if(len(chain) >= len(dh_kk_parameters)):
            raise ValueError("'chain' not coherent with 'dh_kk_parameters'.")
                
    

    def update_q_in_T(self, q):
        """
        Update the joint values in the transformation matrix.

        Args:
            q (list): List of joint values.

        Returns:
            list: Updated DH parameters with joint values.

        """
        for i, c in enumerate(self.chain):
            if c == 'R':        
                self.dh_kk_parameters[i][2] = q[i]      
            elif c == 'P':        
                self.dh_kk_parameters[i][3] = q[i]   
        return self.dh_kk_parameters
                
                                
    
        
    def DH_KK_T(self, alpha, a, theta, r):
        """
        Compute the DH Khalil-Kleinfinger transformation matrix.

        Parameters:
        - alpha:    The DH parameter alpha.
        - a:        The DH parameter a.
        - theta:    The DH parameter theta.
        - r:        The DH parameter r.

        Returns:
        - T:        The DH Khalil-Kleinfinger transformation matrix (numpy array).

        """

        #TODO student: Compute T for "one line" of the DH table
        
        T = np.array([
            [np.NaN,    np.NaN,    np.NaN,      np.NaN],
            [np.NaN,    np.NaN,    np.NaN,      np.NaN],
            [np.NaN,    np.NaN,    np.NaN,      np.NaN],
            [np.NaN,    np.NaN,    np.NaN,      np.NaN]
        ])
        
        
        return T       
        
        

    def DH_KK_ToTn(self,dh_kk_parameters):
            """
            Compute the forward kinematics given Khalil-Kleinfinger parameters.

            Parameters:
            - dh_kk_parameters (list): A list of tuples containing the Khalil-Kleinfinger parameters (alpha, a, theta, r) for each joint.

            Returns:
            - T (numpy.ndarray): The transformation matrix representing the end-effector pose.

            """
            T = np.eye(4)

            #TODO student: Compute T out of T0 T1 ... Tn

            return T
     


   