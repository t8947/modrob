#!/usr/bin/env python

""" Scara Model

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Raphael LEBER"
__copyright__ = "Copyright 2024, CPE Lyon"

import numpy as np
from math import sqrt
import sys


# if __package__ is None or __package__ == '':
# else:
from .scara_num_solver import ScaraNumSolver
from .tools.calc import Calc
from .trajectory import Trajectory


class ScaraModel():

    def __init__(self):
        
        # Trajectories
        self.inter_pol1 = Trajectory("inter_pol1")
        self.inter_pol3 = Trajectory("inter_pol3")
        self.inter_pol5 = Trajectory("inter_pol5")
        self.inter_bang = Trajectory("inter_bang")
        self.inter_trap = Trajectory("inter_trap")
        self.inter_sync = Trajectory("inter_sync")        
        
        self.previous_q = None
        self.previous_t = None        
        
        # Angles (rad)
        self.theta1 = 1
        self.theta2 = -1
        
        # Angles speed (rad/s)
        self.theta1_p = 0
        self.theta2_p = 0
        
        # Angles speed (rad/s2)
        self.theta1_pp = 0
        self.theta2_pp = 0      
        
        self.x, self.y = [0, 0]  
        
        self.t = 0
        
        # Links size (m)
        self.L0_x = 0
        self.L1_x = 8.625 # à changer
        self.L2_x = 6.125 # à changer
        
        # Joints min max
        self.bounds_theta1 = ( 0, np.pi )
        self.bounds_theta2 = ( -np.pi , np.pi  )
        
        # Numerical solver
        self.num_mgi_solver = ScaraNumSolver(self.L1_x, self.L2_x)

        # Derivatives
        self.calc_qp = Calc()
        self.calc_qpp = Calc()
        
        
        
    # Check if theta1 and theta2 are in bounds    
    def is_in_bounds(self, th1, th2):     
        return (th1 >= self.bounds_theta1[0] and th1 <= self.bounds_theta1[1] and th2 >= self.bounds_theta2[0] and th2 <= self.bounds_theta2[1])
    
    # Saturate theta1 and theta2 to bounds if out of bounds
    def sat_to_bounds(self, th1, th2):
        return min( max(th1, self.bounds_theta1[0]), self.bounds_theta1[1] )   , min( max(th2, self.bounds_theta2[0]), self.bounds_theta2[1] )         


    def MGD(self, q):
        """
        Forward Kinematics (Modèle Géométrique Direct)
        
        Calculates the end effector position given the joint angles.
        
        Args:
            q (list): List of joint angles [theta1, theta2].
            
        Returns:
            X (list): List of end effector position [x, y].
        """
        theta1 = q[0]
        theta2 = q[1]
        x1 = self.L1_x * np.cos(theta1)
        y1 = self.L1_x * np.sin(theta1)
        x2 = x1 + self.L2_x * np.cos(theta1 + theta2)
        y2 = y1 + self.L2_x * np.sin(theta1 + theta2)
        X = [x2, y2]
        return X
    

    def MGI(self, X):
        """
        Invert Kinematics (Modèle Géométrique Inverse)
        
        Calculates the joint angles given end effector position.
        
        Args:
            X (list): [x, y] effector coordinates.
            
        Returns:
            q (list): List of joint angles [theta1, theta2].
        """        
        
        #TODO student: Implement the inverse geometric kinematics method
        
        return 
        


    def MCD(self, q, Vq):
        """
        Calculates the velocity of the end effector given the joint angles and joint velocities (Modèle Cinématique Direct)

        Args:
            q (list): List of joint angles [theta1, theta2].
            Vq (list): List of joint velocities [Vtheta1, Vtheta2].

        Returns:
            tuple: Tuple containing the velocity of the end effector (Vx, Vy).
        """

        #TODO student: Implement the direct velocity kinematics method

        return (0, 0)



    def MCI(self, q, V):
        """
        Calculates Inverse Velocity Kinematics method (Modèle Cinématique Inverse) given the joint angles and end-effector velocities.

        Args:
            q (list): List of joint angles [theta1, theta2].
            V (list): List of end-effector velocities [Vx, Vy].

        Returns:
            list: List of joint velocities [qp1, qp2].
        """

        #TODO student: Implement the inverse velocity kinematics method
        
        return [0, 0]
    


    def update_trajectory(self, type, t):
        
        inter_func = {
                "inter_pol1":   self.inter_pol1,
                "inter_pol3":   self.inter_pol3,
                "inter_pol5":   self.inter_pol5,
                "inter_bang":   self.inter_bang,
                "inter_trap":   self.inter_trap,
                "inter_sync":   self.inter_sync
            } 
        
        q, qp, qpp = inter_func[type].update_trajectory(t)       
        
        tf_max = max( inter_func[type].tf ) 
        
        return q, qp, qpp, tf_max
            
                