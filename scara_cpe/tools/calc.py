#!/usr/bin/env python

""" Calc

Calculation tools like derivate

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.
This program is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
this program. If not, see <http://www.gnu.org/licenses/>.
"""

__author__ = "Raphael LEBER"
__copyright__ = "Copyright 2024, CPE Lyon"



import numpy

class Calc():
    
    def __init__(self):
        self.prev_data = None
        self.prev_t = None
        
    def deriv(self, data, t):
        
        if self.prev_data is not None and self.prev_t is not None:
            delta_data = data - self.prev_data
            delta_t = t - self.prev_t
            data_p = delta_data / delta_t  # Calcul de la dérivée de q entre q(n) et q(n-1)
        else:
            data_p = numpy.zeros_like(data)         
            
        
        self.prev_data = data
        self.prev_t = t        
        
        return data_p     
    
    def reset(self):
        self.prev_data = None
        self.prev_t = None        
                