# Modélisation des robots

## Intro

### Objectif et contexte
L’objectif de ce TP est de mettre en pratique les bases de la modélisation des robots. Durant les séances de TP, vous parcourrez les techniques d’obtention des modèles géométriques et cinématiques, et de génération de mouvement. Ce TP s’appuie sur un simulateur d’un robot SCARA à deux degrés de liberté que vous serez amenés à manipuler en langage python.

![scara_demo](assets/images/scara_demo.gif){width=450px}

<!--
<div  id="Figure-2" style="text-align: center;">
    <img src="assets/images/scara_demo.gif"
        style="width: 450px" 
    />
</div>
-->

### Répartition du travail  


Vous pouvez travailler en monôme ou en binôme (trinôme interdit). Les monômes/binômes resterons les mêmes pour toute la durée du module. Votre travail en TP sera évalué à hauteur de 50% du module.  


![binomes](assets/images/binomes.gif){width=300px}

<!--
<div  id="Figure-2" style="text-align: center;">
    <img src="assets/images/binomes.gif"
        style="width: 300px; text-align: center;" 
    />
</div>
-->


### Organisation des rendus

Certaines parties (avec le logo :memo:) sont à faire "sur papier", d'autres parties (avec le logo :computer:) sont a faire directement sur ordinateur. Les parties "papiers" seront à intégrer (scan/photo) dans le rendu numérique.

Le rendu numérique se fait sous GIT. A cet effet un repo GITLAB sera généré par le prof pour chaque binôme. Chaque séance devra faire l'objet d'un ou plusieurs commits. Chaque partie de TP devra faire l'objet d'un [TAG GIT](https://stackoverflow.com/questions/18216991/create-a-tag-in-a-github-repository) qui fera office de rendu. 

Consigne du rendu :
- Votre repo GIT de rendu est fourni par le prof
- Le repo GIT contiendra :
    - Vos codes selon l'organisation décrite [plus loin](PART0%20-%20Description.md)
    - Un fichier README.md dans lequel vous ferez un sommaire pointant vers les différentes parties de TP
    - Un dossier `assets/images` contenant les illustrations et photos/scan de vos parties papier (:memo:)
    - Un fichier par partie (PART1.md, PART2.md, ...) contenant :
        - Vos explications
        - Vos équations / calculs (scan/photos ou [LaTeX](https://ashki23.github.io/markdown-latex.html#latex))
        - Eventuellement des [extraits de codes](https://www.freecodecamp.org/news/how-to-format-code-in-markdown/) pertinents
        - Vos résultats (captures d'écrans idéalement) et les conclusions à en tirer

:warning: Si vous clonez un repo GIT dans votre repo GIT, il faudra bien supprimer le dossier `.git` présent dans le "sous-repo" GIT.

:warning: Il vous appartient de vérifier que les rendus (commits, tags) soient bien rendus (push)


# Présentation de l'environnement de travail

## Présentation du simulateur

Le simulateur est composé de 3 fenêtres :
- "Robot Display" : Affiche le robot scara en 2D 
- "Control Panel" : L'interface de commande pour l'utilisateur
- "Curves" : Affiche les courbes suite à une action demandée au bras

<!--
<div  id="Figure-1" style="text-align: center;">
    <img src="assets/images/RobotDisplay.png"
        style="width: 30%; max-width: 100%;" 
    />
    <img src="assets/images/ControlPanel.png"
        style="width: 30%; max-width: 100%;" 
    />
    <img src="assets/images/Curves.png"
        style="width: 30%; max-width: 100%;" 
    />        
</div>
-->

![RobotDisplay](assets/images/RobotDisplay.png){width=30%}
![ControlPanel](assets/images/ControlPanel.png){width=30%}
![Curves](assets/images/Curves.png){width=30%}  


### Robot Display

- Affiche le robot scara en 2D 
- La zone verte représente l'espace accessible du robot
- Si vous cliquez dans cette fenêtre, cela permet de déplacer l'effecteur aux coordonnées de la souris si celle-ci est dans l'espace accessible, ou au plus proche si le click se fait en dehors de l'espace accessible.  

### Control Panel

La première partie concerne le modèle géométrique :  
- Le bouton "Get robot $ \theta_1 \theta_2 $" permet de récupérer les variables articulaires depuis "Robot Display"
- Le bouton "Get robot x y" permet de récupérer les coordonnées opérationnelles depuis "Robot Display"
- Les "sliders" de gauche permettent de définir un angle
- Les "sliders" de droite permettent de définir une position d'effecteur
- Le bouton "MGD ->" permet d'executer la fonction MGD de `scara_model.py` en prenant les valeurs des sliders de gauche
- Le bouton "<- MGI" permet d'executer la fonction MGI de `scara_model.py` en prenant les valeurs des sliders de droite


## Comment installer le simulateur

Pour installer le simulateur, faites :

```bash
git clone git@gitlab.com:t8947/modrob.git
cd modrob
poetry install
```

:warning: Si le `git clone` ne fonctionne pas, assurez-vous [d'avoir une clef SSH](https://docs.gitlab.com/ee/user/ssh.html), car le HTTPS n'est pas compatible avec la suite du TP.

Pour lancer le simulateur:  

```bash
poetry run python -m scara_cpe.scara_cpe
# Ancienne version (plus valable) : poetry run scara_cpe/scara_cpe.py
```

## Comment coder dans le simulateur

### Naviguer dans les fichiers et les modifiers

Fichiers auxquels vous ne devez pas toucher :
- `scara_plot.py(c)` : Gère l'affichage dans les 3 fenêtres


Fichiers que vous serez amenés à modifier / compléter :
- `robot_model.py` : Fonctions de transformations rigides valables pour n'importe quel robot articulé série
- `scara_model.py` : Fonctions de transformations rigides valables pour le robot scara_cpe
- `scara_cpe.py` : Programme principal du robot scara cpe
- `scara_num_solver.py` : Programme du solveur numérique du robot scara cpe
- `trajectory.py` : Programme d'interpolation de trajectoires pour robots séries


### Mettre vos modifications sur votre repo GIT

Une fois pour toute, ajoutez un 2ème repo distant à votre projet :

```bash
git remote add student <URL-@git-de-votre-repo.git>
```

Puis plus tard quand vous ferez des `push`, veillez bien à faire 
```bash
git push student <nom-de-votre-branche> 
```

... même principe pour les `pull` ou `fetch`


Pour faire une mise à jour du simulateur, faites:
```bash
git pull origin  
```


### Comment lancer les tests unitaires

Dans un terminal, allez dans le dossier du projet puis tapez :
```bash
poetry run pytest -v # -v sert à afficher plus de détails (vebose)
# Ancienne commande à éxecuter dans scara_cpe toujours valable :  poetry run pytest -v ..
```

Si tout est vert... c'est bien !
Si il y a du rouge... il y a des erreurs dans votre code


# Énoncé

... donné au format papier uniquement...

