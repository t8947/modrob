import unittest
import numpy as np

from scara_cpe.robot_model import RobotModel


class TestRobotModel(unittest.TestCase):
    
    def test__DH_KK_T__1(self):
            
        # DH KK parameters
        dh_kk_parameters = [
            (0,         0,          0,          0),
            (-np.pi/2,  0,          -np.pi/2,   0),
            (-np.pi/2,  1,          0,          0),
            (np.pi/2,   0,          0,          0),
            (0,         1,          0,          1),
        ]
            
        # robot model instance
        RPRR_model = RobotModel("RPRR", dh_kk_parameters)

        result = RPRR_model.DH_KK_T(*dh_kk_parameters[4])

        expected_result = np.array(
            [   [ 1,  0,  0,  1 ],
                [ 0,  1,  0,  0 ],
                [ 0,  0,  1,  1 ],
                [ 0,  0,  0,  1 ]   ]
        )

        assert np.allclose(result, expected_result)

        
            

    def test__DH_KK_ToTn__1(self):
        
        # DH KK parameters
        dh_kk_parameters = [
            (0,         0,          0,          0),
            (-np.pi/2,  0,          -np.pi/2,   0),
            (-np.pi/2,  1,          0,          0),
            (np.pi/2,   0,          0,          0),
            (0,         1,          0,          1),
        ]
            
        # robot model instance
        RPRR_model = RobotModel("RPRR", dh_kk_parameters)

        result = RPRR_model.DH_KK_ToTn(dh_kk_parameters)

        expected_result = np.array(
            [   [ 0,  1,  0,  0 ],
                [ 0,  0,  1,  1 ],
                [ 1,  0,  0,  2 ],
                [ 0,  0,  0,  1 ]   ]
        )

        assert np.allclose(result, expected_result)




if __name__ == '__main__':
    unittest.main()
